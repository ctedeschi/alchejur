(ns alchejur.core-test
  (:require [clojure.test :refer :all]
            [alchejur.core.exec :refer :all]))


" test 1 - arity 2: getmax "

(defn getmax-condition [v]
  (>= (first v) (second v)))

(defn getmax-product [v]
  (conj [] (reduce max v)))

(def getmax-mset [0 1 3 -3 -10 1 7])

(deftest getmax-test
  (is (= [7] 
        (exec getmax-mset 2 getmax-condition getmax-product))))


" test 2 - arity 2: eratho "

(defn eratho-condition [v]
  (= 0 (mod (first v) (second v))))

(defn eratho-product [v]
  (conj [] (second v)))

(def eratho-mset (into [] (range 2 50)))

(deftest eratho-test
  (is (= (set [2 3 5 7 11 13 17 19 23 29 31 37 41 43 47])
         (set (exec eratho-mset 2 eratho-condition eratho-product)))))

" test 3 - arity 1: multseven "

(defn multseven-condition [v] 
  (not (= 0 (mod (first v) 7))))

(defn multseven-product [v] 
  [])

(def multseven-mset (into [] (range 50)))

(deftest multseven-test
  (is (= (set [0 7 14 21 28 35 42 49])
         (set (exec multseven-mset 1 multseven-condition multseven-product)))))


" test 4 - arity 3: pipo "

(defn pipo-arity3-condition [v]
  (not (= (nth v 2) (* (first v) (second v)))))

(defn pipo-arity3-product [v]
  [])

(def pipo-arity3-mset [2 30 20 600 3 6])

(deftest pipo-arity3-test
  (is (= []
         (exec pipo-arity3-mset 3 pipo-arity3-condition pipo-arity3-product))))


