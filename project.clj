(defproject alchejur "0.1.0-SNAPSHOT"
  :description "A Clojure-based chemical programming interpreter"
  :url "http://people.irisa.fr/Cedric.Tedeschi/"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]])
