
;; An example of chemical program using the Alchejur library
(ns alchejur.core.examples.getmax-alchejur
  (:require [alchejur.core.exec :refer :all]))

;; getmax-condition ([vector]) represents the rule's condition.
;;  returns true if the first element of vector is greater than
;;  or equal to the second.
(defn getmax-condition [v]
  (>= (first v) (second v)))

;; getmax-product ([vector]) represents the product of the rule.
;; returns the higher value of the (two) values in vector.
(defn getmax-product [v]
  (conj [] (reduce max v)))

;; the mset
(def getmax-mset [0 1 3 -3 -10 1 7])

;; executing the program and printing the result
(print (exec getmax-mset 2 getmax-condition getmax-product))


