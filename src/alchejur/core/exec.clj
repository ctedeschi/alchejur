(ns alchejur.core.exec)

""" ***************** """
""" findind reactants """
""" ***************** """

;; combi ([k v]) 
;;  returns a vector containing all combinations of k elts of v "
(defn combi [v k]
    (if (= 1 k)
        (into [] (map vector v))
        (if (< (count v) k)
            []
            (into []
              (concat
                (combi (drop 1 v) k)
                (map (fn [x] (conj x (first v)))
                (combi (drop 1 v) (dec k))))))))

;; add-elt-everywhere ([v elt])
;;  returns a vector containing all possible concatenation of elt with v 
(defn add-elt-everywhere [v elt]
  (into []
      (for [x (range (count v)) y (range (+ (count (nth v x)) 1))]
        (into [] (concat (subvec  (nth v x) 0 y)
                (conj [] elt)
                (subvec (nth v x) y (count (nth v x))))))))

;; permute ([mset])
;;  returns a vector of every full permutation of mset "
(defn permute [mset]
  (if (= (count mset) 1)
    (map vector mset)
    (if (= (count mset) 2)
      (add-elt-everywhere (conj [] (subvec mset 0 1)) (second mset))
      (add-elt-everywhere
        (permute (subvec mset 0 (dec' (count mset))))
        (nth mset (dec' (count mset)))))))

;; k-permute-sorted ([mset k])
;;  returns all permutations of k elts of mset,
;;  every permutation of the same combination in a same sub-vector
(defn k-permute-sorted [mset k]
  (map permute (combi mset k)))

;; k-permute ([mset k])
;;  returns all permutations of k elts of mset, in a flat vector
(defn k-permute [mset k]
  (def ps (k-permute-sorted mset k))
  (into [] (for [x (range (count ps)) y (range (count (nth ps x)))]
    (nth (nth ps x) y))))

;; find-reactants ([mset arity condition])
;;  find reactants satisfying the arity and the condition "
(defn find-reactants [mset arity condition]
  (into [] (first (filter condition (k-permute mset arity)))))


""" ***************** """
""" updating the mset """
""" ***************** """

;; remove-first-occ ([e mset])
;;  remove the first occurence of e from mset
(defn remove-first-occ [e mset]
  (concat
    (take-while #(not= % e) mset)
    (rest (drop-while #(not= % e) mset))))

;; update-mset ([mset toremove toadd])
;;  remove the toremove coll and add the toadd coll
;;  from / to the mset coll "
(defn update-mset [mset toremove toadd]
  (into [] (concat
    (loop [ms mset tor toremove toa toadd pos 0]
      (if (= pos (count tor))
        ms
        (recur (remove-first-occ (nth toremove pos) ms)
                tor toa (inc' pos))))
    toadd)))

""" ********************* """
""" executing the program """
""" ********************* """

;; exec ([mset arity condition product)]
;;  returns a vector containing the elements of an inert mset
;;  produced by repeatedly trying to find elements in the mset satisfying
;;  the condition that are, once found replaced by their product.
(defn exec [mset arity condition product]
  (if (= [] (find-reactants mset arity condition))
  mset
  (exec (update-mset
          mset
          (find-reactants mset arity condition)
          (product (find-reactants mset arity condition)))
        arity condition product)))


